{
    // get eosws_token from: https://dfuse.io/#subscribe
    "logLevel": "debug",
    "listenPort": 8800,
    "ackInterval": 100,
    "logCompletedBlockInterval": 10000,
    "db": {
        "host": "localhost",
        "port": "27017",
        "user": "eosWriter",
        "password": "eosWriter",
        "dbName": "EOS"
    },
    "eosNode": {
        protocol: "https",
        host: "telos.caleos.io",
        port: 443,
    },
    "filters": {
        /*
            Actions filter examples:
            "contractName": true All actions of the contract
            "contractName": {
                action1: true,
            } Specified actions of the contract
        */
        "actions": {
            "accts.seeds": true,
            "harvst.seeds": true,
            "settgs.seeds": true,
            "funds.seeds": true,
            "invite.seeds": true,
            "join.seeds": true,
            "rules.seeds": true,
            "histry.seeds": true,
            "policy.seeds": true,
            "token.seeds": true,
            "free.seeds": true,
            "gifts.seeds": true,
            "milest.seeds": true,
            "hypha.seeds": true,
            "allies.seeds": true,
            "refer.seeds": true,
            "bank.seeds": true,
            "system.seeds": true,
            "forum.seeds": true,
            "orgs.seeds": true,
        },
        /*
            Tables filter examples:
            "contractName": true All tables of the contract including all scopes
            "contractName": {
                table: true,
            } Specified tables of the contract including all scopes
            "contractName": {
                table: {
                    scope: true
                },
            } Specified tables of the contract with the specified scopes
        */
        "tables": {
            "accts.seeds": {
                "refs":true,
                "reputation":true,
                "cbs":true,
                "users":true,
                "vouch":true,
                "vouches":true,
                "trxstat":true,
                "config":true,
                "rep":true,
                "reqvouch":true,
                "sizes":true,
                "transactions":true,
            },
            "harvst.seeds": {
                "config": true,
                "balances": true,
                "refunds": true,
                "reputation": true,
                "users": true,
                "trxstat": true,
                "harvest": true,
                "cbs": true,
                "cspoints": true,
                "planted": true,
                "txpoints": true,
                "sizes": true,
                "total": true,
                "rep": true,
                "transactions": true,
                // "cycle": true,
            },
            "settgs.seeds": {
                "config":true,
                "contracts":true,
            },
            "funds.seeds": {
                "config": true,
                "props": true,
                "users": true,
                "votes": true,
                "voice": true,
                "lastprops": true,
                "cycle": true,
                "participants": true,
                "minstake": true,
            },
            "invite.seeds": {
                "sponsors": true,
            },
            "join.seeds": {
                "invites": true,
                "sponsors": true,
                "referrers": true,
            },
            "rules.seeds": {
                "voters": true,
                "balances": true,
                "referendums": true,
                "config": true,
            },
            "histry.seeds": {
                "citizens": true,
                "residents": true,
                "history": true,
                "transactions": true,
                "users": true,
                "config": true,
            },
            "policy.seeds": {
                "policies": true,
                "policiesnew": true,
                "devicepolicy": true,
            },
            "token.seeds": {
                "accounts": true,
                "stat": true,
                "trxstat": true,
                "circulating": true,
                "config": true,
            },
            // "gifts.seeds": true,
            // "milest.seeds": true,
            // "hypha.seeds": true,
            // "allies.seeds": true,
            // "refer.seeds": true,
            // "bank.seeds": true,
            // "system.seeds": true,
            "forum.seeds": {
                "postcomment": true,
                "vote": true,
                "forumrep": true,
                "users": true,
                "votepower": true,
                "config": true,
                "operations": true,
            },
            "orgs.seeds": {
                "organization": true,
                "members": true,
                "sponsors": true,
                "votes": true,
                "config": true,
                "apps": true,
                "dauhistory": true,
                "daus": true,
            },
        }
    },
    "tablePrimaryKeys": {
        "accts.seeds": {
            "refs": "invited",
            "reputation": "account",
            "cbs": "account",
            "users": "account",
            "vouch": "sponsor",
            "vouches": "id",
            "trxstat": "transactions_volume",
            "config": "param",
            "rep": "account",
            "reqvouch": "id",
            "sizes":"id",
            "transactions":"id"
        },
        "forum.seeds": {
            "postcomment": "id",
            "vote": "account",
            "forumrep": "account",
            "users": "account",
            "votepower": "account",
            "config": "param",
            "operations": "operation"
        },
        "harvst.seeds": {
            "config": "param",
            "balances": "account",
            "refunds": "refund_id",
            "reputation": "account",
            "users": "account",
            "trxstat": "transactions_volume",
            "harvest": "account",
            "cbs": "account",
            "cspoints": "account",
            "planted": "account",
            "txpoints": "account",
            "sizes":"id",
            "total":"id",
            "rep": "account",
            "transactions":"id",
            "cycle":"cycle_id"
        },
        "histry.seeds": {
            "citizens": "id",
            "residents": "id",
            "history": "history_id",
            "transactions": "id",
            "users": "account",
            "config": "param"
        },
        "invite.seeds": {
            "sponsors": "account"
        },
        "join.seeds": {
            "invites": "invite_id",
            "sponsors": "account",
            "referrers": "invite_id"
        },
        "orgs.seeds": {
            "organization": "org_name",
            "members": "account",
            "sponsors": "account",
            "votes": "account",
            "config": "param",
            "apps": "app_name",
            "dauhistory": "dau_history_id",
            "daus":"account"
        },
        "policy.seeds": {
            "policies": "account",
            "policiesnew": "account",
            "devicepolicy": "id"
        },
        "funds.seeds": {
            "config": "param",
            "props": "id",
            "users": "account",
            "votes": "account",
            "voice": "account",
            "lastprops": "account",
            "cycle": "propcycle",
            "participants": "account",
            "minstake": "prop_id"
        },
        "rules.seeds": {
            "voters": "account",
            "balances": "account",
            "referendums": "referendum_id",
            "config": "param"
        },
        "schdlr.seeds": {
            "operations": "operation",
            "config": "param"
        },
        "settgs.seeds": {
            "config": "param",
            "contracts": "contract"
        },
        "token.seeds": {
            "accounts": "balance",
            "stat": "supply",
            "trxstat": "account",
            "circulating":"id",
            "config":"param"
        }
    }
}