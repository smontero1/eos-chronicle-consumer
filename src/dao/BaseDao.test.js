/* eslint-disable no-undef */
const { DbClient } = require('../service');
const { BaseDao } = require('../dao');

jest.setTimeout(20000);

let dbClient = null;
let baseDao = null;

beforeAll(async () => {
  dbClient = new DbClient({
    "host": "localhost",
    "port": "27017",
    "user": "eosWriter",
    "password": "eosWriter",
    "dbName": "EOS"
  });
  await dbClient.connect();
});

afterAll(async () => {
  await dbClient.close();
});

beforeEach(async () => {
  try {
    await dbClient.dropCollection('test-collection');
  } catch (error) {
  }
  baseDao = new BaseDao('test-collection', dbClient);
  await baseDao.createCollection();
});

describe('Test insertMany', () => {
  test('insert one', async () => {
    await baseDao.insertMany(getItems(1));
    const results = await baseDao.findByIdsMapped([1]);
    expect(results).not.toBeNull();
    expect(results).toHaveProperty('1');
    expect(results['1']).toEqual({
      _id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });
  });

  test('insert multiple', async () => {
    await baseDao.insertMany(getItems(3));
    const results = await baseDao.findByIdsMapped([1, 2, 3]);
    expect(results).not.toBeNull();
    expect(results).toHaveProperty('1');
    expect(results).toHaveProperty('2');
    expect(results).toHaveProperty('3');
    expect(results['1']).toEqual({
      _id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });
    expect(results['2']).toEqual({
      _id: 2,
      prop1: 'prop12',
      prop2: 'prop22',
    });
    expect(results['3']).toEqual({
      _id: 3,
      prop1: 'prop13',
      prop2: 'prop23',
    });
  });
});


describe('Test bulkWrite', () => {
  beforeEach(async () => {
    await insertItems(10);
  });

  test('each operation', async () => {
    baseDao.addInsertOneToBulk(
      {
        _id: 11,
        prop1: 'prop111',
        prop2: 'prop211',
      }
    );

    baseDao.addReplaceOneToBulk(
      {
        _id: 1,
        prop1: 'prop11r',
        prop2: 'prop21r',
      }
    );

    baseDao.addManyReplaceOneToBulk([
      {
        _id: 2,
        prop1: 'prop12r',
        prop2: 'prop22r',
      },
      {
        _id: 3,
        prop1: 'prop13r',
        prop2: 'prop23r',
      }
    ]);

    baseDao.addDeleteManyByIdToBulk([4, 5]);
    baseDao.addDeleteByIdToBulk(7);
    baseDao.addDeleteItemsToBulk(getItems(10, 9));
    await baseDao.bulkWrite();
    const results = await baseDao.findByIdsMapped(getIds(11));

    expect(results).not.toBeNull();
    expect(results).toHaveProperty('1');
    expect(results['1']).toEqual({
      _id: 1,
      prop1: 'prop11r',
      prop2: 'prop21r',
    });
    expect(results).toHaveProperty('2');
    expect(results['2']).toEqual({
      _id: 2,
      prop1: 'prop12r',
      prop2: 'prop22r',
    });
    expect(results).toHaveProperty('2');
    expect(results['3']).toEqual({
      _id: 3,
      prop1: 'prop13r',
      prop2: 'prop23r',
    });
    expect(results).not.toHaveProperty('4');
    expect(results).not.toHaveProperty('5');
    expect(results).toHaveProperty('6');
    expect(results['6']).toEqual({
      _id: 6,
      prop1: 'prop16',
      prop2: 'prop26',
    });
    expect(results).not.toHaveProperty('7');
    expect(results).toHaveProperty('8');
    expect(results['8']).toEqual({
      _id: 8,
      prop1: 'prop18',
      prop2: 'prop28',
    });
    expect(results).not.toHaveProperty('9');
    expect(results).not.toHaveProperty('10');
    expect(results).toHaveProperty('11');
    expect(results['11']).toEqual({
      _id: 11,
      prop1: 'prop111',
      prop2: 'prop211',
    });

  });
});

async function insertItems(end, start = 1) {
  await baseDao.insertMany(getItems(end, start));
}

function getItems(end, start = 1) {
  const items = [];
  for (let i = start; i <= end; i++) {
    items.push({
      _id: i,
      prop1: `prop1${i}`,
      prop2: `prop2${i}`,
    });
  }
  return items;
}

function getIds(end, start = 1) {
  const ids = [];
  for (let i = start; i <= end; i++) {
    ids.push(i);
  }
  return ids;
}
