const { Util } = require('../util');

class BaseDao {

  /**
   * 
   * @param {string} tableName 
   * @param {DbClient} dbClient 
   */
  constructor(tableName, dbClient, opts) {
    opts = opts || {
      insertBatchSize: 1000
    };
    const {
      insertBatchSize
    } = opts;
    this.tableName = tableName;
    this.dbClient = dbClient;
    this.bulkArray = [];
    this.bulkDeleteIds = [];
    this.insertBatchItems = [];
    this.insertBatchSize = insertBatchSize;

  }

  async createCollection() {
    if (!await this.collectionExists()) {
      await this.dbClient.createCollection(this.tableName);
      this.collection = this.dbClient.collection(this.tableName);
      await this.createIndexes();
    } else {
      this.collection = this.dbClient.collection(this.tableName);
    }
  }

  async dropCollection() {
    if (await this.collectionExists()) {
      await this.dbClient.dropCollection(this.tableName);
    }
  }

  async recreateCollection() {
    await this.dropCollection();
    await this.createCollection();
  }

  /**
   * For subclasses to create indexes on table creation
   */
  async createIndexes() { }

  async collectionExists() {
    return this.dbClient.collectionExists(this.tableName);
  }

  async createIndex(field, opts) {
    return this.collection.createIndex(field, opts);
  }


  find(query, opts) {
    return this.collection.find(query, opts);
  }

  async findByIds(ids) {
    ids = Util.removeDuplicates(ids);
    return this.find({
      _id: {
        $in: ids
      }
    }).toArray();
  }

  async findAll() {
    return this.find().toArray();
  }

  async findByIdsMapped(ids) {
    const items = await this.findByIds(ids);
    return Util.arrayToMap(items, '_id');
  }

  addInsertOneToBulk(item) {
    this.bulkArray.push({
      insertOne: {
        document: item,
      }
    });
  }

  addReplaceOneToBulk(item, filter = null) {
    if (!filter) {
      filter = {
        _id: item._id
      };
    }
    this.bulkArray.push({
      replaceOne: {
        filter,
        replacement: item,
        upsert: true,
      }
    });
  }

  addManyReplaceOneToBulk(items, filter = null) {
    for (const item of items) {
      this.addReplaceOneToBulk(item, filter);
    }
  }

  addDeleteManyByIdToBulk(ids) {
    ids = Util.removeDuplicates(ids);
    this.bulkArray.push({
      deleteMany: {
        filter: {
          _id: {
            $in: ids
          }
        }
      }
    });
  }

  addDeleteByIdToBulk(id) {
    this.bulkDeleteIds.push(id);
  }

  addDeleteItemsToBulk(items) {
    let ids = Util.propToArray(items, '_id');
    this.addDeleteManyByIdToBulk(ids);
  }

  async insertBatch(item) {
    this.insertBatchItems.push(item);
    if (this.insertBatchItems.length > this.insertBatchSize) {
      await this.flushInsertBatch();
    }
  }

  async flushInsertBatch() {
    const items = this.insertBatchItems;
    this.insertBatchItems = [];
    await this.insertMany(items);
  }

  async insertMany(items, opts) {
    if (items.length) {
      return this.collection.insertMany(items, opts);
    }
  }

  async bulkWrite(opts) {
    if (this.bulkDeleteIds.length) {
      this.addDeleteManyByIdToBulk(this.bulkDeleteIds);
      this.bulkDeleteIds = [];
    }
    if (!this.bulkArray.length) {
      return;
    }
    const ops = this.bulkArray;
    this.bulkArray = [];
    //console.log(this.tableName, JSON.stringify(ops, null, 4));
    return this.collection.bulkWrite(ops, opts);
  }
}

module.exports = BaseDao;