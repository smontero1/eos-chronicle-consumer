const BaseDao = require('./BaseDao');
const ChangeLogDao = require('./ChangeLogDao');

module.exports = {
  BaseDao,
  ChangeLogDao,
};