const BaseDao = require('./BaseDao');


class ChangeLogDao extends BaseDao {

  async createIndexes() {
    this.createIndex('block_num');
  }

  findFirstInvalidChangePerId(blockNum) {
    return this.collection.aggregate([
      {
        $match: { block_num: { $gt: blockNum } }
      },
      {
        $sort: { block_num: 1 }
      },
      {
        $group: {
          _id: "$item_id",
          item_id: { $first: "$item_id" },
          block_num: { $first: "$block_num" },
          op: { $first: "$op" },
          old: { $first: "$old" },
          new: { $first: "$new" },
        }
      }
    ]).toArray();
  }

  deleteIrreversible(irreversibleBlockNum) {
    return this.collection.deleteMany(
      {
        block_num: { $lte: irreversibleBlockNum }
      }
    );
  }

  deleteReversed(forkBlockNum, opts) {
    return this.collection.deleteMany(
      {
        block_num: { $gt: forkBlockNum }
      },
      opts
    );
  }

}

module.exports = ChangeLogDao;