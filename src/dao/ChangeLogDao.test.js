/* eslint-disable no-undef */
const { DbClient } = require('../service');
const { ChangeLogDao } = require('.');

jest.setTimeout(20000);

let dbClient = null;
let changeLogDao = null;

beforeAll(async () => {
  dbClient = new DbClient({
    "host": "localhost",
    "port": "27017",
    "user": "eosWriter",
    "password": "eosWriter",
    "dbName": "EOS"
  });
  await dbClient.connect();
});

afterAll(async () => {
  await dbClient.close();
});

beforeEach(async () => {
  try {
    await dbClient.dropCollection('test-collection');
  } catch (error) {
  }
  changeLogDao = new ChangeLogDao('test-collection', dbClient);
  await changeLogDao.createCollection();
});

describe('Test deleteIrreversible', () => {
  beforeEach(async () => {
    await insertItems(1, 5, 3);
    await insertItems(2, 8, 7);
  });
  test('delete none', async () => {
    await changeLogDao.deleteIrreversible(2);
    let ids = [];
    ids = ids.concat(getIds(1, 5, 3));
    ids = ids.concat(getIds(2, 8, 7));
    const results = await changeLogDao.findByIdsMapped(ids);
    expect(results).not.toBeNull();
    expect(results).toHaveProperty('1-3');
    expect(results).toHaveProperty('1-4');
    expect(results).toHaveProperty('1-5');
    expect(results).toHaveProperty('2-7');
    expect(results).toHaveProperty('2-8');
  });

  test('delete one', async () => {
    await changeLogDao.deleteIrreversible(3);
    let ids = [];
    ids = ids.concat(getIds(1, 5, 3));
    ids = ids.concat(getIds(2, 8, 7));
    const results = await changeLogDao.findByIdsMapped(ids);
    expect(results).not.toBeNull();
    expect(results).not.toHaveProperty('1-3');
    expect(results).toHaveProperty('1-4');
    expect(results).toHaveProperty('1-5');
    expect(results).toHaveProperty('2-7');
    expect(results).toHaveProperty('2-8');
  });

  test('delete many', async () => {
    await changeLogDao.deleteIrreversible(7);
    let ids = [];
    ids = ids.concat(getIds(1, 5, 3));
    ids = ids.concat(getIds(2, 8, 7));
    const results = await changeLogDao.findByIdsMapped(ids);
    expect(results).not.toBeNull();
    expect(results).not.toHaveProperty('1-3');
    expect(results).not.toHaveProperty('1-4');
    expect(results).not.toHaveProperty('1-5');
    expect(results).not.toHaveProperty('2-7');
    expect(results).toHaveProperty('2-8');
  });

});

describe('Test deleteReversed', () => {
  beforeEach(async () => {
    await insertItems(1, 5, 3);
    await insertItems(2, 8, 7);
  });
  test('delete none', async () => {
    await changeLogDao.deleteReversed(8);
    let ids = [];
    ids = ids.concat(getIds(1, 5, 3));
    ids = ids.concat(getIds(2, 8, 7));
    const results = await changeLogDao.findByIdsMapped(ids);
    expect(results).not.toBeNull();
    expect(results).toHaveProperty('1-3');
    expect(results).toHaveProperty('1-4');
    expect(results).toHaveProperty('1-5');
    expect(results).toHaveProperty('2-7');
    expect(results).toHaveProperty('2-8');
  });

  test('delete one', async () => {
    await changeLogDao.deleteReversed(7);
    let ids = [];
    ids = ids.concat(getIds(1, 5, 3));
    ids = ids.concat(getIds(2, 8, 7));
    const results = await changeLogDao.findByIdsMapped(ids);
    expect(results).not.toBeNull();
    expect(results).toHaveProperty('1-3');
    expect(results).toHaveProperty('1-4');
    expect(results).toHaveProperty('1-5');
    expect(results).toHaveProperty('2-7');
    expect(results).not.toHaveProperty('2-8');
  });

  test('delete many', async () => {
    await changeLogDao.deleteReversed(4);
    let ids = [];
    ids = ids.concat(getIds(1, 5, 3));
    ids = ids.concat(getIds(2, 8, 7));
    const results = await changeLogDao.findByIdsMapped(ids);
    expect(results).not.toBeNull();
    expect(results).toHaveProperty('1-3');
    expect(results).toHaveProperty('1-4');
    expect(results).not.toHaveProperty('1-5');
    expect(results).not.toHaveProperty('2-7');
    expect(results).not.toHaveProperty('2-8');
  });

});

describe('Test findFirstInvalidChangePerId', () => {
  beforeEach(async () => {
    await insertItems(1, 5, 3);
    await insertItems(2, 8, 7);
    await insertItems(3, 6, 6);
  });
  test('None invalid changes', async () => {
    const results = await changeLogDao.findFirstInvalidChangePerId(8);
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);
  });

  test('One Invalid Change', async () => {
    const results = await changeLogDao.findFirstInvalidChangePerId(7);
    expect(results).not.toBeNull();
    expect(results).toHaveLength(1);
    expect(results[0]).toEqual({
      _id: 2,
      item_id: 2,
      block_num: 8,
      op: 'insert',
      old: {
        _id: 2,
        prop1: `old18`,
        prop2: `old28`,
      },
      new: {
        _id: 2,
        prop1: `new18`,
        prop2: `new28`,
      }
    });
  });

  test('Many invalid changes', async () => {
    const results = await changeLogDao.findFirstInvalidChangePerId(3);
    expect(results).not.toBeNull();
    expect(results).toHaveLength(3);
    expect(new Set(results)).toEqual(new Set([
      {
        _id: 1,
        item_id: 1,
        block_num: 4,
        op: 'insert',
        old: {
          _id: 1,
          prop1: `old14`,
          prop2: `old24`,
        },
        new: {
          _id: 1,
          prop1: `new14`,
          prop2: `new24`,
        }
      },
      {
        _id: 2,
        item_id: 2,
        block_num: 7,
        op: 'insert',
        old: {
          _id: 2,
          prop1: `old17`,
          prop2: `old27`,
        },
        new: {
          _id: 2,
          prop1: `new17`,
          prop2: `new27`,
        }
      },
      {
        _id: 3,
        item_id: 3,
        block_num: 6,
        op: 'insert',
        old: {
          _id: 3,
          prop1: `old16`,
          prop2: `old26`,
        },
        new: {
          _id: 3,
          prop1: `new16`,
          prop2: `new26`,
        }
      }
    ]));

  });
});

async function insertItems(itemId, end, start = 1) {
  await changeLogDao.insertMany(getItems(itemId, end, start));
}

function getItems(itemId, end, start = 1) {
  const items = [];
  for (let i = start; i <= end; i++) {
    items.push({
      _id: `${itemId}-${i}`,
      item_id: itemId,
      block_num: i,
      op: 'insert',
      old: {
        _id: itemId,
        prop1: `old1${i}`,
        prop2: `old2${i}`,
      },
      new: {
        _id: itemId,
        prop1: `new1${i}`,
        prop2: `new2${i}`,
      }
    });
  }
  return items;
}

function getIds(itemId, end, start = 1) {
  const ids = [];
  for (let i = start; i <= end; i++) {
    ids.push(`${itemId}-${i}`);
  }
  return ids;
}
