const { BaseDao, ChangeLogDao } = require('../dao');
const { DbOps } = require('../const');
const { Util } = require('../util');
const { tablePrimaryKeys } = require('config')

class TableManager {

  constructor(tableName, dbClient) {
    this.dbClient = dbClient;
    this.dao = new BaseDao(tableName, dbClient);
    this.changeLogDao = new ChangeLogDao(tableName + '-change-log', dbClient);
    const { account, table } = TableManager.parseTableName(tableName)
    this.account = account
    this.table = table
    this.primaryKey = TableManager.getTablePrimaryKey(account, table)
  }

  static getTableName(account, table) {
    return `${account}-${table}`;
  }

  static parseTableName(tableName) {
    const [account, table] = tableName.split('-')
    return {
      account,
      table
    };
  }

  static getTablePrimaryKey(account, table) {
    if (!tablePrimaryKeys[account][table]) {
      throw `No primary key for table ${account}-${table}`;
    }
    return tablePrimaryKeys[account][table];
  }

  static restructureItem(scope, primaryKey, data) {
    return {
      _id: `${scope}-${data[primaryKey]}`,
      _scope: scope,
      ...data
    };
  }


  async init() {
    await this.dao.createCollection();
    await this.changeLogDao.createCollection();
  }

  getUpdateFn({
    blockNum,
    deltas,
    session,
    irreversible = false
  }) {
    deltas = this._restuctureDeltas(deltas);
    deltas = this._keepLatestById(deltas);
    const { adds, removes } = this._splitByOp(deltas);
    return async () => {
      console.log(`In Update Fn blockNum: ${blockNum} irreversible: ${irreversible} deltas: `, JSON.stringify(deltas, null, 4));
      if (!irreversible) {
        let changeLog = await this._generateAddChangeLog(adds, blockNum);
        changeLog = changeLog.concat(this._generateRemoveChangeLog(removes, blockNum));
        console.log(`Change log: `, JSON.stringify(changeLog, null, 4));
        await this.changeLogDao.insertMany(changeLog, { session });
      }
      this.dao.addManyReplaceOneToBulk(adds);
      this.dao.addDeleteItemsToBulk(removes);
      await this.dao.bulkWrite({ session });
    };
  }

  async getRollbackFn(forkBlockNum, session) {
    const changes = await this.changeLogDao.findFirstInvalidChangePerId(forkBlockNum);
    //console.log('Changes:', changes);
    for (const change of changes) {
      switch (change.op) {
        case DbOps.DELETE:
          this.dao.addInsertOneToBulk(change.old);
          break;
        case DbOps.INSERT:
          this.dao.addDeleteByIdToBulk(change.item_id);
          break;
        case DbOps.UPDATE:
          this.dao.addReplaceOneToBulk(change.old);
      }
    }
    return async () => {
      await this.dao.bulkWrite({ session });
      await this.changeLogDao.deleteReversed(forkBlockNum, { session });
    };
  }

  async deleteIrreversible(irreversibleBlockNum) {
    return this.changeLogDao.deleteIrreversible(irreversibleBlockNum);
  }

  async _generateAddChangeLog(adds, blockNum) {
    let changeLog = [];
    let ids = Util.propToArray(adds, '_id');
    const toUpdate = await this.dao.findByIdsMapped(ids);
    for (const item of adds) {
      let change = null;
      const {
        _id,
      } = item;
      if (toUpdate[_id]) {
        change = {
          op: DbOps.UPDATE,
          old: toUpdate[_id],
          new: item,
        };
      } else {
        change = {
          op: DbOps.INSERT,
          new: item,
        };
      }
      change.item_id = _id;
      change.block_num = blockNum;
      changeLog.push(change);
    }
    return changeLog;
  }

  _generateRemoveChangeLog(removes, blockNum) {
    let changeLog = [];
    for (const item of removes) {
      changeLog.push({
        op: DbOps.DELETE,
        item_id: item._id,
        block_num: blockNum,
        old: item,
      });
    }
    return changeLog;
  }

  _restuctureDeltas(deltas) {
    const restructured = [];
    for (const delta of deltas) {
      const {
        added,
        kvo: {
          scope,
          value
        }
      } = delta;

      restructured.push({
        added,
        item: TableManager.restructureItem(scope, this.primaryKey, value),
      });
    }
    return restructured;
  }

  _keepLatestById(deltas) {
    const map = {};
    for (const delta of deltas) {
      map[delta.item._id] = delta;
    }
    return Object.values(map);
  }

  _splitByOp(deltas) {
    const adds = [];
    const removes = [];
    for (const delta of deltas) {
      const {
        added,
        item,
      } = delta;
      if (added === 'true') {
        adds.push(item);
      } else {
        removes.push(item);
      }
    }
    return {
      adds,
      removes,
    };
  }
}

module.exports = TableManager;