const DbUpdater = require('./DbUpdater');
const TableManager = require('./TableManager');

module.exports = {
  DbUpdater,
  TableManager,
};