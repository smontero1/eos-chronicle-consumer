/* eslint-disable no-undef */
const { DbClient } = require('../service');
const { DbUpdater } = require('.');
const { BaseDao } = require('../dao');

jest.setTimeout(20000);

let dbClient = null;
let dbUpdater = null;

beforeAll(async () => {
  dbClient = new DbClient({
    "host": "localhost",
    "port": "27017",
    "user": "eosWriter",
    "password": "eosWriter",
    "dbName": "EOS"
  });
  await dbClient.connect();
  dbUpdater = new DbUpdater(dbClient);
  await dbClient.dropDb()
  dbClient.createDb();
});

afterAll(async () => {
  await dbClient.close();
});

describe('Integration test', () => {

  test('Integration test', async () => {

    const table1BaseDao = new BaseDao('acc1-table1', dbClient);
    await table1BaseDao.createCollection();
    const table1ChangeLogBaseDao = new BaseDao('acc1-table1-change-log', dbClient);
    await table1ChangeLogBaseDao.createCollection();
    const table2BaseDao = new BaseDao('acc1-table2', dbClient);
    await table2BaseDao.createCollection();
    const table2ChangeLogBaseDao = new BaseDao('acc1-table2-change-log', dbClient);
    await table2ChangeLogBaseDao.createCollection();
    const table3BaseDao = new BaseDao('acc2-table3', dbClient);
    await table3BaseDao.createCollection();
    const table3ChangeLogBaseDao = new BaseDao('acc2-table3-change-log', dbClient);
    await table3ChangeLogBaseDao.createCollection();

    let deltas = {
      'acc1-table1': [
        {
          added: 'true',
          block_num: 1,
          kvo: {
            scope: 1,
            value: {
              id: 1,
              prop1: 'prop11',
              prop2: 'prop21',
            }
          }
        },
        {
          added: 'true',
          block_num: 1,
          kvo: {
            scope: 1,
            value: {
              id: 2,
              prop1: 'prop12',
              prop2: 'prop22',
            }
          }
        },
      ],
      'acc1-table2': [
        {
          added: 'true',
          block_num: 1,
          kvo: {
            scope: 2,
            value: {
              id: 1,
              prop1: 'prop11',
              prop2: 'prop21',
            }
          }
        },
      ]
    };
    await dbUpdater.processDeltas(1, 2, deltas);



    let results = await table1BaseDao.findByIdsMapped([
      '1-1',
      '1-2',
    ]);
    expect(results).not.toBeNull();
    expect(results).toHaveProperty('1-1');
    expect(results['1-1']).toEqual({
      _id: '1-1',
      _scope: 1,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });
    expect(results['1-2']).toEqual({
      _id: '1-2',
      _scope: 1,
      id: 2,
      prop1: 'prop12',
      prop2: 'prop22',
    });

    results = await table1ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);


    results = await table2BaseDao.findByIdsMapped([
      '2-1',
    ]);

    expect(results).not.toBeNull();
    expect(results).toHaveProperty('2-1');
    expect(results['2-1']).toEqual({
      _id: '2-1',
      _scope: 2,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });

    results = await table2ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);

    deltas = {
      'acc1-table1': [
        {
          added: 'true',
          block_num: 3,
          kvo: {
            scope: 1,
            value: {
              id: 1,
              prop1: 'prop11m',
              prop2: 'prop21m',
            }
          }
        },
        {
          added: 'false',
          block_num: 3,
          kvo: {
            scope: 1,
            value: {
              id: 2,
              prop1: 'prop12',
              prop2: 'prop22',
            }
          }
        },
      ],
      'acc2-table3': [
        {
          added: 'true',
          block_num: 3,
          kvo: {
            scope: 3,
            value: {
              id: 1,
              prop1: 'prop11',
              prop2: 'prop21',
            }
          }
        },
      ]
    };

    await dbUpdater.processDeltas(3, 1, deltas);

    results = await table1BaseDao.findByIdsMapped([
      '1-1',
      '1-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('1-1');
    expect(results['1-1']).toEqual({
      _id: '1-1',
      _scope: 1,
      id: 1,
      prop1: 'prop11m',
      prop2: 'prop21m',
    });
    expect(results).not.toHaveProperty('1-2');

    results = await table1ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(2);

    results = await table2ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);

    results = await table3BaseDao.findByIdsMapped([
      '3-1',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('3-1');
    expect(results['3-1']).toEqual({
      _id: '3-1',
      _scope: 3,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });

    results = await table3ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(1);

    await dbUpdater.rollback(1);

    results = await table1BaseDao.findByIdsMapped([
      '1-1',
      '1-2',
    ]);
    expect(results).not.toBeNull();
    expect(results).toHaveProperty('1-1');
    expect(results['1-1']).toEqual({
      _id: '1-1',
      _scope: 1,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });
    expect(results['1-2']).toEqual({
      _id: '1-2',
      _scope: 1,
      id: 2,
      prop1: 'prop12',
      prop2: 'prop22',
    });

    results = await table1ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);


    results = await table2BaseDao.findByIdsMapped([
      '2-1',
    ]);

    expect(results).not.toBeNull();
    expect(results).toHaveProperty('2-1');
    expect(results['2-1']).toEqual({
      _id: '2-1',
      _scope: 2,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });

    results = await table2ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);


    results = await table3BaseDao.findByIdsMapped([
      '3-1',
    ]);

    expect(results).not.toBeNull();
    expect(results).not.toHaveProperty('3-1');

    results = await table3ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);


    deltas = {
      'acc1-table1': [
        {
          added: 'false',
          block_num: 5,
          kvo: {
            scope: 1,
            value: {
              id: 1,
              prop1: 'prop11',
              prop2: 'prop21',
            }
          }
        },
        {
          added: 'true',
          block_num: 5,
          kvo: {
            scope: 1,
            value: {
              id: 2,
              prop1: 'prop12m',
              prop2: 'prop22m',
            }
          }
        },
      ],
      'acc1-table2': [
        {
          added: 'true',
          block_num: 5,
          kvo: {
            scope: 2,
            value: {
              id: 2,
              prop1: 'prop12',
              prop2: 'prop22',
            }
          }
        }
      ],
      'acc2-table3': [
        {
          added: 'true',
          block_num: 5,
          kvo: {
            scope: 3,
            value: {
              id: 1,
              prop1: 'prop11',
              prop2: 'prop21',
            }
          }
        },
      ]
    };

    await dbUpdater.processDeltas(5, 2, deltas);

    results = await table1BaseDao.findByIdsMapped([
      '1-1',
      '1-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).not.toHaveProperty('1-1');
    expect(results).toHaveProperty('1-2');
    expect(results['1-2']).toEqual({
      _id: '1-2',
      _scope: 1,
      id: 2,
      prop1: 'prop12m',
      prop2: 'prop22m',
    });

    results = await table1ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(2);

    results = await table2BaseDao.findByIdsMapped([
      '2-1',
      '2-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('2-1');
    expect(results['2-1']).toEqual({
      _id: '2-1',
      _scope: 2,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });
    expect(results).toHaveProperty('2-2');
    expect(results['2-2']).toEqual({
      _id: '2-2',
      _scope: 2,
      id: 2,
      prop1: 'prop12',
      prop2: 'prop22',
    });

    results = await table2ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(1);

    results = await table3BaseDao.findByIdsMapped([
      '3-1',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('3-1');
    expect(results['3-1']).toEqual({
      _id: '3-1',
      _scope: 3,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });

    results = await table3ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(1);


    deltas = {
      'acc1-table1': [
        {
          added: 'true',
          block_num: 6,
          kvo: {
            scope: 1,
            value: {
              id: 2,
              prop1: 'prop12m2',
              prop2: 'prop22m2',
            }
          }
        },
      ],
      'acc1-table2': [
        {
          added: 'false',
          block_num: 6,
          kvo: {
            scope: 2,
            value: {
              id: 2,
              prop1: 'prop12',
              prop2: 'prop22',
            }
          }
        }
      ],
      'acc2-table3': [
        {
          added: 'true',
          block_num: 6,
          kvo: {
            scope: 3,
            value: {
              id: 2,
              prop1: 'prop12',
              prop2: 'prop22',
            }
          }
        },
      ]
    };

    await dbUpdater.processDeltas(6, 3, deltas);

    results = await table1BaseDao.findByIdsMapped([
      '1-1',
      '1-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).not.toHaveProperty('1-1');
    expect(results).toHaveProperty('1-2');
    expect(results['1-2']).toEqual({
      _id: '1-2',
      _scope: 1,
      id: 2,
      prop1: 'prop12m2',
      prop2: 'prop22m2',
    });

    results = await table1ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(3);

    results = await table2BaseDao.findByIdsMapped([
      '2-1',
      '2-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('2-1');
    expect(results['2-1']).toEqual({
      _id: '2-1',
      _scope: 2,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });
    expect(results).not.toHaveProperty('2-2');

    results = await table2ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(2);

    results = await table3BaseDao.findByIdsMapped([
      '3-1',
      '3-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('3-1');
    expect(results['3-1']).toEqual({
      _id: '3-1',
      _scope: 3,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });

    expect(results).toHaveProperty('3-2');
    expect(results['3-2']).toEqual({
      _id: '3-2',
      _scope: 3,
      id: 2,
      prop1: 'prop12',
      prop2: 'prop22',
    });

    results = await table3ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(2);

    deltas = {
      'acc1-table1': [
        {
          added: 'false',
          block_num: 7,
          kvo: {
            scope: 1,
            value: {
              id: 2,
              prop1: 'prop12m2',
              prop2: 'prop22m2',
            }
          }
        },
      ],
      'acc1-table2': [
        {
          added: 'true',
          block_num: 7,
          kvo: {
            scope: 2,
            value: {
              id: 1,
              prop1: 'prop11m',
              prop2: 'prop21m',
            }
          }
        }
      ],
      'acc2-table3': [
        {
          added: 'true',
          block_num: 7,
          kvo: {
            scope: 3,
            value: {
              id: 2,
              prop1: 'prop12m',
              prop2: 'prop22m',
            }
          }
        },
      ]
    };

    await dbUpdater.processDeltas(7, 4, deltas);

    results = await table1BaseDao.findByIdsMapped([
      '1-1',
      '1-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).not.toHaveProperty('1-1');
    expect(results).not.toHaveProperty('1-2');

    results = await table1ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(4);

    results = await table2BaseDao.findByIdsMapped([
      '2-1',
      '2-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('2-1');
    expect(results['2-1']).toEqual({
      _id: '2-1',
      _scope: 2,
      id: 1,
      prop1: 'prop11m',
      prop2: 'prop21m',
    });
    expect(results).not.toHaveProperty('2-2');

    results = await table2ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(3);

    results = await table3BaseDao.findByIdsMapped([
      '3-1',
      '3-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('3-1');
    expect(results['3-1']).toEqual({
      _id: '3-1',
      _scope: 3,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });

    expect(results).toHaveProperty('3-2');
    expect(results['3-2']).toEqual({
      _id: '3-2',
      _scope: 3,
      id: 2,
      prop1: 'prop12m',
      prop2: 'prop22m',
    });

    results = await table3ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(3);


    await dbUpdater.rollback(5);

    results = await table1BaseDao.findByIdsMapped([
      '1-1',
      '1-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).not.toHaveProperty('1-1');
    expect(results).toHaveProperty('1-2');
    expect(results['1-2']).toEqual({
      _id: '1-2',
      _scope: 1,
      id: 2,
      prop1: 'prop12m',
      prop2: 'prop22m',
    });

    results = await table1ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(2);

    results = await table2BaseDao.findByIdsMapped([
      '2-1',
      '2-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('2-1');
    expect(results['2-1']).toEqual({
      _id: '2-1',
      _scope: 2,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });
    expect(results).toHaveProperty('2-2');
    expect(results['2-2']).toEqual({
      _id: '2-2',
      _scope: 2,
      id: 2,
      prop1: 'prop12',
      prop2: 'prop22',
    });

    results = await table2ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(1);

    results = await table3BaseDao.findByIdsMapped([
      '3-1',
      '3-2',
    ]);
    expect(results).not.toBeNull();

    expect(results).toHaveProperty('3-1');
    expect(results['3-1']).toEqual({
      _id: '3-1',
      _scope: 3,
      id: 1,
      prop1: 'prop11',
      prop2: 'prop21',
    });
    expect(results).not.toHaveProperty('3-2');

    results = await table3ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(1);

    await dbUpdater.deleteIrreversible(5);

    results = await table1ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);

    results = await table2ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);

    results = await table3ChangeLogBaseDao.findAll();
    expect(results).not.toBeNull();
    expect(results).toHaveLength(0);

  });

});


async function insertItems(itemId, end, start = 1) {
  await dbUpdater.insertMany(getItems(itemId, end, start));
}

function getItems(itemId, end, start = 1) {
  const items = [];
  for (let i = start; i <= end; i++) {
    items.push({
      _id: `${itemId}-${i}`,
      item_id: itemId,
      block_num: i,
      old: {
        _id: itemId,
        prop1: `old1${i}`,
        prop2: `old2${i}`,
      },
      new: {
        _id: itemId,
        prop1: `new1${i}`,
        prop2: `new2${i}`,
      }
    });
  }
  return items;
}

function getIds(itemId, end, start = 1) {
  const ids = [];
  for (let i = start; i <= end; i++) {
    ids.push(`${itemId}-${i}`);
  }
  return ids;
}
