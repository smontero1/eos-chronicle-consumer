const TableManager = require('./TableManager');

const transactionOptions = {
  readPreference: 'primary',
  readConcern: { level: 'local' },
  writeConcern: { w: 'majority' }
};

class DbUpdater {
  constructor(dbClient) {
    this.dbClient = dbClient;
    this.tableManagers = {};
  }

  async rollback(forkBlockNum) {
    const session = this.dbClient.startSession();
    try {
      const rollbackFns = [];
      for (const tableName in this.tableManagers) {
        rollbackFns.push(await this.tableManagers[tableName].getRollbackFn(forkBlockNum, session));
      }
      await this._executeInTransaction(rollbackFns, session);
    } catch (error) {
      console.error('Error occurred while rolling back changes: ', error);
      throw error;
    } finally {
      await session.endSession();
    }
  }

  async processDeltas(blockNum, lastIrreversible, deltas) {
    const session = this.dbClient.startSession();
    try {
      const updateFns = [];
      for (const tableName in deltas) {
        if (!this.tableManagers[tableName]) {
          const tableManager = new TableManager(tableName, this.dbClient);
          this.tableManagers[tableName] = tableManager;
          await tableManager.init();

        }
        const tableManager = this.tableManagers[tableName];
        updateFns.push(
          tableManager.getUpdateFn(
            {
              blockNum,
              deltas: deltas[tableName],
              session,
              irreversible: blockNum <= lastIrreversible,
            }
          ));
      }
      await this._executeInTransaction(updateFns, session);
    } catch (error) {
      console.error('Error occurred while updating tables: ', error);
      throw error;
    } finally {
      await session.endSession();
    }

  }

  async deleteIrreversible(irreversibleBlockNum) {
    try {
      const promises = [];
      for (const tableName in this.tableManagers) {
        promises.push(this.tableManagers[tableName].deleteIrreversible(irreversibleBlockNum));
      }
      await Promise.all(promises);
    } catch (error) {
      console.error('Error occurred while deleting irreversible change logs: ', error);
      throw error;
    }
  }

  async _executeInTransaction(fns, session) {
    await session.withTransaction(async () => {
      for (const fn of fns) {
        await fn();
      }
    }, transactionOptions);
  }
}

module.exports = DbUpdater;