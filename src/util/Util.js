class Util {
    static isEmptyObj(obj) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) return false;
        }
        return true;
    }

    static isString(value) {
        return typeof value === 'string' || value instanceof String;
    }

    static removeDuplicates(values) {
        values = Array.isArray(values) ? values : [values];
        return [...new Set(values)];
    }

    static arrayToMap(objs, keyProp) {
        const map = {};
        for (const obj of objs) {
            let keys = obj[keyProp];
            keys = Array.isArray(keys) ? keys : [keys];
            for (const key of keys) {
                map[key] = obj;
            }
        }
        return map;
    }

    static propToArray(objs, prop) {
        const values = [];
        for (const obj of objs) {
            values.push(obj[prop]);
        }
        return values;
    }
}

module.exports = Util;
