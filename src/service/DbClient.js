const { MongoClient } = require('mongodb');

class DbClient {
  constructor({
    host,
    port,
    user,
    password,
    dbName,
  }) {
    this.host = host;
    this.port = port;
    this.user = user;
    this.password = encodeURIComponent(password);
    this.dbName = dbName;
  }

  async connect() {
    try {
      console.log('Connecting to database ...');
      this.client = await MongoClient.connect(`mongodb://${this.user}:${this.password}@${this.host}:${this.port}/${this.dbName}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });

      console.log('Connected to database.');
      this.db = this.client.db(this.dbName);
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  createDb() {
    this.db = this.client.db(this.dbName);
  }

  async dropDb() {
    await this.db.dropDatabase();
    this.db = null;
  }

  async dropCollection(collection) {
    await this.collection(collection).drop();
  }

  collection(collection) {
    return this.db.collection(collection);
  }

  async createCollection(name, opts) {
    return this.db.createCollection(name, opts);
  }

  async collectionExists(name) {

    const cursor = this.db.listCollections({
      name,
    }, {
      nameOnly: true
    });
    const exists = await cursor.hasNext();
    await cursor.close();
    return exists;

  }

  startSession() {
    return this.client.startSession();
  }

  async close() {
    try {
      console.log('Closing connection to db...');
      this.client && await this.client.close();
      console.log('Closed connection to db.');
    } catch (error) {
      console.warn("Error closing connection to database", error);
    }

  }
}

module.exports = DbClient;