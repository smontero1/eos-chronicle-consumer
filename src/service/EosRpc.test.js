const eosRpc = require('./EosRpc');

jest.setTimeout(20000);

describe('Get Abi', () => {
  test('Get accts.seeds', async () => {
    const abi = await eosRpc.getAbi('accts.seeds');
    expect(abi).toBeDefined();
    console.log(JSON.stringify(abi, null, 4));
  });
});

describe('Get Abi', () => {
  test('Get allies.seeds', async () => {
    const abi = await eosRpc.getAbi('allies.seeds');
    expect(abi).toBeDefined();
    console.log(JSON.stringify(abi, null, 4));
  });
});

/* describe('Get Code', () => {
  test('accts.seeds', async () => {
    const code = await eosRpc.getCode('accts.seeds');
    expect(code).toBeDefined();
    console.log(code);
  });
}); */

/* describe('getTables', () => {
  test('accts.seeds', async () => {
    const tables = await eosRpc.getTables('accts.seeds');
    expect(tables).toBeInstanceOf(Array);
    console.log(tables);
  });
});

describe('getTableNames', () => {
  test('accts.seeds', async () => {
    const tables = await eosRpc.getTableNames('accts.seeds');
    expect(tables).toBeInstanceOf(Array);
    console.log(tables);
  });
});

describe('getScopeValues', () => {
  test('token.seeds accounts', async () => {
    const scopeValues = await eosRpc.getTableScopeValues('token.seeds', 'accounts');
    expect(scopeValues).toBeInstanceOf(Array);
    console.log(scopeValues);
  });
});


describe('Get table by scope', () => {
  test('token.seeds accounts', async () => {
    let scopes = await eosRpc.getTableByScope({
      code: 'token.seeds',
      table: 'accounts',
      lowerBound: null,
      limit: 10
    });
    expect(scopes).toBeDefined();
    console.log(scopes);

    scopes = await eosRpc.getTableByScope({
      code: 'token.seeds',
      table: 'accounts',
      lowerBound: scopes.more,
      limit: 10
    });
    expect(scopes).toBeDefined();
    console.log(scopes);
  });
});

describe('Get table rows', () => {
  test('accts.seeds users', async () => {
    let rows = await eosRpc.getTableRows({
      code: 'accts.seeds',
      table: 'users',
      limit: 5
    });
    expect(rows).toBeDefined();
    console.log(rows);
  });
});
 */

/* describe('Get table rows', () => {
  test('join.seeds invites', async () => {
    let rows = null;
    let more = null;
    let lowerBound = null;
    do {
      ({ rows, more } = await eosRpc.getTableRows({
        code: 'join.seeds',
        table: 'invites',
        lowerBound,
        limit: 5
      }));
      if (lowerBound) {
        rows.shift();
      }
      if (more) {
        lowerBound = rows[rows.length - 1].invite_id;
      }
      console.log(rows);
    } while (more);
    expect(rows).toBeDefined();
  });
}); */