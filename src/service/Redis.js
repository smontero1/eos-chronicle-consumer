class Redis {
  constructor({
    client,
    prefix
  }) {
    this.client = client;
    this.prefix = prefix;
    this.setKey = `${this.prefix}:app`;
    this.purgeSetKey = `${this.setKey}:purge`;

  }

  async set(key, value, purgable = true) {
    await this.client.hset(this._getSetKey(purgable), key, this._stringify(value));
  }

  async get(key, purgable = true) {
    const value = await this.client.hget(this._getSetKey(purgable), key);
    return this._parse(value);
  }

  /* async lpush(key, value, purgable = true) {
    await this.client.lpush(this._fullKey(key, purgable), this._stringify(value));
  }

  async lpop(key, purgable = true) {
    const value = await this.client.lpop(this._fullKey(key, purgable));
    return this._parse(value);
  }

  async lindex(key, index, purgable = true) {
    const value = await this.client.lindex(this._fullKey(key, purgable), index);
    return this._parse(value);
  } */

  _stringify(value) {
    return JSON.stringify(value);
  }

  _parse(value) {
    return JSON.parse(value);
  }

  _getSetKey(purgable = true) {
    return purgable ? this.purgeSetKey : this.setKey;
  }
}

module.exports = Redis;