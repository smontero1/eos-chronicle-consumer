const { JsonRpc } = require('eosjs');
const fetch = require('node-fetch');
const config = require('config');


class EosRpc {
    constructor() {
        const {
            eosNode: {
                host,
                port,
                protocol,
            }
        } = config;

        this.rpc = new JsonRpc(
            `${protocol}://${host}:${port}`,
            { fetch }
        );
    }

    async getAbi(account) {
        return this.rpc.get_abi(account);
    }

    async getAbi(account) {
        return this.rpc.get_abi(account);
    }

    async getCode(account) {
        return this.rpc.get_code(account);
    }

    async getTables(account) {
        const { abi: { tables } } = await this.getAbi(account);
        return tables;
    }

    async getTableNames(account) {
        const names = [];
        const tables = await this.getTables(account);
        for (const table of tables) {
            names.push(table.name);
        }
        return names;
    }

    async getTableScopeValues(code, table, limit = 10000) {
        const scopeValues = [];
        let rows;
        let lowerBound = null;
        do {
            ({ rows, more: lowerBound } = await this.getTableByScope({
                code,
                table,
                lowerBound,
                limit,
            }));

            for (const row of rows) {
                scopeValues.push(row.scope);
            }
        } while (lowerBound);
        return scopeValues;
    }

    async getTableByScope(
        {
            code,
            table,
            lowerBound,
            upperBound,
            limit = 10,
        }
    ) {
        return this.rpc.get_table_by_scope({
            code,
            table,
            lower_bound: lowerBound,
            upper_bound: upperBound,
            limit,
        });
    }

    async getTableRows(
        {
            code,
            scope,
            table,
            indexPosition,
            tableKey,
            lowerBound,
            upperBound,
            keyType,
            limit = 10,
            reverse,
        }
    ) {
        scope = scope || code;

        return this.rpc.get_table_rows({
            json: true,
            code,
            scope,
            table,
            index_position: indexPosition,
            table_key: tableKey,
            lower_bound: lowerBound,
            upper_bound: upperBound,
            key_type: keyType,
            limit,
            reverse,
        });
    }

    async getOneTableRow(
        {
            code,
            scope,
            table,
            indexPosition,
            tableKey,
            upperBound,
            keyType,
            lowerBound,
            reverse,
        }
    ) {
        const { rows } = await this.getTableRows(
            {
                code,
                scope,
                table,
                indexPosition,
                tableKey,
                lowerBound,
                upperBound,
                keyType,
                reverse,
                limit: 1,
            }
        );
        return rows.length ? rows[0] : null;
    }
}

module.exports = new EosRpc();
