module.exports = {
  ACTION: 'action',
  BLOCK: 'block',
  BLOCK_COMPLETED: 'blockCompleted',
  CONNECTED: 'connected',
  DISCONNECTED: 'disconnected',
  FORK: 'fork',
  TABLE_ROW: 'tableRow',
  TX: 'tx',
};