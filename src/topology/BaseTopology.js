const straw = require('@smontero/straw');
const yargs = require('yargs');

class BaseTopolgy {

    constructor(prefix, {
        redis,
    }) {
        redis = redis || {};

        this.argv = yargs.option('purge', {
            alias: 'p',
            description: 'Purge pipes',
            type: 'boolean',
        }).option('delete', {
            alias: 'd',
            description: 'delete purgable items',
            type: 'boolean',
        }).argv;
        console.log("Should purge: ", this.argv.purge);
        console.log("Should delete purgable items: ", this.argv.delete);
        this.purge = this.argv.purge;

        this.opts = {
            nodes_dir: __dirname + '/../node',
            redis: {
                host: '127.0.0.1',
                port: 6379,
                prefix,
                ...redis
            },
            purgeKeys: 'app:purge',
            purgeAppItems: this.argv.delete,
        };


    }

    async start() {
        await this._create(await this.getNodes(), this.opts);
    }

    async getNodes() {
        //Base class must override this method
    }

    async _create(nodes, opts) {
        var topo = await straw.create(opts);
        topo.add(nodes, () => {
            topo.start({ purge: this.purge });
        });

        var destroyFn = function () {
            console.log('Destroying topoplogy.');
            topo.destroy(function () {
                console.log('Finished.');
            });
        }
        process.on('SIGINT', destroyFn);
        process.on('SIGTERM', destroyFn);

        this.topology = topo;
    }
}

module.exports = BaseTopolgy;

