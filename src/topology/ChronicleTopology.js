
const BaseTopology = require('./BaseTopology');
const config = require('config');

class ChronicleTopolgy extends BaseTopology {


    async getNodes() {
        let nodes = [{
            id: 'chronicle-listener',
            node: 'ChronicleListener',
            outputs: {
                'msg': 'msg',
            },
            config,
        },
        {
            id: 'filter',
            node: 'Filter',
            input: 'msg',
            outputs: {
                'filtered-msg': 'filtered-msg',
            },
            config,
        },
        {
            id: 'group-in-blocks',
            node: 'GroupInBlocks',
            input: 'filtered-msg',
            outputs: {
                'actions': 'actions',
                'table-deltas': 'table-deltas',
            },
        },
        {
            id: 'actions-logger',
            node: 'Logger',
            input: 'actions',
        },
        {
            id: 'update-tables',
            node: 'UpdateTables',
            input: 'table-deltas',
            config,
        }];
        return nodes;
    }
}

new ChronicleTopolgy('chronicle-topology', {}).start();

