const straw = require('@smontero/straw');
const ConsumerServer = require('chronicle-consumer');
const { MsgType } = require('../const');
const { Redis } = require('../service');

module.exports = straw.node({
    async initialize(opts, done) {
        this.opts = opts;
        const {
            redis,
            config: {
                listenPort,
                ackInterval,
                logCompletedBlockInterval,
            },
        } = opts;
        console.log(`Listen Port: ${listenPort}`);
        this.redis = new Redis(redis);
        this.listenPort = listenPort;
        this.ackInterval = ackInterval;
        this.logCompletedBlockInterval = logCompletedBlockInterval;
        this.consumerServer = new ConsumerServer({
            port: listenPort,
            ackEvery: ackInterval
        });
        this.subscribeToEvents(this.consumerServer);
        done();
    },

    start(done) {
        console.log("Starting ChronicleListener");
        this.consumerServer.start();
        console.log("Started ChronicleListener");
        done(false);
    },

    async stop(done) {
        console.log("Stopping ChronicleListener...");
        this.consumerServer.server.close();
        done(false);
    },

    subscribeToEvents(server) {

        server.on(MsgType.FORK, (data) => {
            this.outputMsg(MsgType.FORK, data);
        });

        server.on(MsgType.TX, (data) => {
            this.processTx(data);
        });

        server.on(MsgType.TABLE_ROW, (data) => {
            //console.log('Table delta: ', data)
            this.outputMsg(MsgType.TABLE_ROW, data);
        });

        server.on(MsgType.BLOCK_COMPLETED, (data) => {
            const {
                block_num,
            } = data;
            if (block_num % this.logCompletedBlockInterval === 0) {
                console.log('Block completed: ', JSON.stringify(data, null, 4));
            }

            this.outputMsg(MsgType.BLOCK_COMPLETED, data);
        });

        server.on(MsgType.CONNECTED, (data) => {
            console.log('CONNECTED: ' + JSON.stringify(data));
        });

        server.on(MsgType.DISCONNECTED, (data) => {
            console.log('DISCONNECTED: ' + JSON.stringify(data));
        });
    },

    processTx(msg) {
        const {
            block_num,
            block_time,
            trace: {
                id: tx_id,
                status,
                action_traces,
            }
        } = msg;

        if (status != 'executed') {
            return;
        }

        for (const trace of action_traces) {
            const {
                receiver,
                act: { account }
            } = trace;
            // TODO: Is this the right way to avoid inline notifications but not skip inline actions?
            if (receiver != account)
                continue

            this.outputMsg(
                MsgType.ACTION,
                {
                    block_num,
                    block_time,
                    tx_id,
                    trace,
                });
        }
    },

    outputMsg(msgType, payload) {
        this.output('msg', {
            msgType,
            payload,
        });
    },

});