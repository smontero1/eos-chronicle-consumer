const straw = require('@smontero/straw');
const { MsgType } = require('../const');

module.exports = straw.node({

  async initialize(opts, done) {
    this.opts = opts;
    const {
      config: {
        filters: {
          actions,
        },
        tablePrimaryKeys
      },
    } = opts;
    this.actions = actions;
    // Using tablePrimaryKeys instead of table filters as we can't load tables without primary keys
    this.tables = tablePrimaryKeys;
    console.log('Wanted tables: ', this.tables);
    done();
  },

  process(msg, done) {

    // if(msg.msgType === MsgType.TABLE_ROW){
    //   console.log('All table messages:', JSON.stringify(msg))
    // }
    if (this.isWanted(msg)) {
      if(msg.msgType === MsgType.TABLE_ROW){
        //console.log('Wanted message:', JSON.stringify(msg))
      }
      this.output('filtered-msg', msg);
    }
    done(false);
  },

  isWanted(msg) {
    const {
      msgType,
      payload
    } = msg;

    switch (msgType) {
      case MsgType.ACTION:
        return this.isWantedAction(payload);
      case MsgType.TABLE_ROW:
        return this.isWantedTable(payload);
      default:
        return true;
    }
  },

  isWantedAction(payload) {
    const {
      trace: {
        act: {
          account,
          name
        }
      }
    } = payload;
    const contractActions = this.actions[account];
    return contractActions && (contractActions === true || contractActions[name]);
  },

  isWantedTable(payload) {
    const {
      kvo: {
        code,
        table,
      }
    } = payload;
    const contractTables = this.tables[code];
    return contractTables && contractTables[table];
  },

});