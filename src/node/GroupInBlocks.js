const straw = require('@smontero/straw');
const { MsgType } = require('../const');
const { TableManager } = require('../domain');
const { Redis } = require('../service');

const CURRENT_BLOCK_STATE = 'current-block-state';

module.exports = straw.node({

  async initialize(opts, done) {
    this.opts = opts;
    const {
      redis,
    } = opts;
    this.redis = new Redis(redis);
    this.blockActions = null;
    this.blockTableDeltas = null;
    this.blockNum = null;
    done();
  },

  async stop(done) {
    console.log("Stopping ForkNavigator...");
    done(false);
  },

  process(msg, done) {
    const {
      msgType,
      payload,
      payload: {
        block_num,
      }
    } = msg;

    if (msgType === MsgType.BLOCK_COMPLETED) {
      this.onBlockCompleted(msg);
      return done(false);
    }

    if (msgType === MsgType.FORK) {
      this.blockNum = null;
      this.outputActions(msg);
      this.outputTableDeltas(msg);
      return done(false);
    }

    if (this.blockNum != null && this.blockNum != block_num) {
      console.warn('Possible issue, block_num changed without a block completed event');
      this.blockNum = null;
    }

    if (!this.blockHasData()) {
      this.startNewBlock(block_num);
    }
    switch (msgType) {
      case MsgType.ACTION:
        this.blockActions.push(payload);
        break;
      case MsgType.TABLE_ROW:
        const tableName = this.getTableName(payload);
        if (!this.blockTableDeltas[tableName]) {
          this.blockTableDeltas[tableName] = [];
        }
        this.blockTableDeltas[tableName].push(payload);
        break;
    }
    done(false);
  },

  getTableName(payload) {
    const {
      kvo: {
        code,
        table,
      }
    } = payload;
    return TableManager.getTableName(code, table);
  },

  blockHasData() {
    return this.blockNum != null;
  },

  startNewBlock(blockNum) {
    this.blockNum = blockNum;
    this.blockActions = [];
    this.blockTableDeltas = {};
  },

  onBlockCompleted(msg) {
    if (this.blockHasData()) {
      const {
        blockActions,
        blockTableDeltas,
      } = this;
      this.blockNum = null;
      this.outputActions(msg, blockActions);
      this.outputTableDeltas(msg, blockTableDeltas);
    }
  },

  outputActions(blockEvent, msgs = null) {
    this.outputMsgs('actions', {
      blockEvent,
      msgs,
    });
  },

  outputTableDeltas(blockEvent, msgs = null) {
    this.outputMsgs('table-deltas', {
      blockEvent,
      msgs,
    });
  },

  outputMsgs(outputKey, {
    msgs,
    blockEvent,
  }) {
    this.output(outputKey, {
      blockEvent,
      msgs
    });
  },

});