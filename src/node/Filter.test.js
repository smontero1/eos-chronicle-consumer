/* eslint-disable no-undef */
const Filter = require('./Filter');
const { MsgType } = require('../const');

jest.setTimeout(20000);

let filterNode = null;
beforeAll(async () => {
  filterNode = new Filter();
  filterNode.initialize(
    {
      config: {
        "filters": {
          "actions": {
            "contract1": true,
            "contract2": {
              "action1": true
            },
          }
        },
        "tablePrimaryKeys": {
          "contract1": true,
          "contract2": {
            "table1": "id1",
            "table2": "id2"
          },
        }
      }
    },
    () => { }
  );
});


describe('Actions filter', () => {
  test('All actions', async () => {
    let isWanted = filterNode.isWantedAction({
      trace: {
        act: {
          account: 'contract1',
          name: 'action1'
        }
      }
    });
    expect(isWanted).toBeTruthy();

    isWanted = filterNode.isWantedAction({
      trace: {
        act: {
          account: 'contract1',
          name: 'action1'
        }
      }
    });
    expect(isWanted).toBeTruthy();

    isWanted = filterNode.isWantedAction({
      trace: {
        act: {
          account: 'contract3',
          name: 'action1'
        }
      }
    });
    expect(isWanted).toBeFalsy();
  });

  test('Specific actions', async () => {
    let isWanted = filterNode.isWantedAction({
      trace: {
        act: {
          account: 'contract2',
          name: 'action1'
        }
      }
    });
    expect(isWanted).toBeTruthy();

    isWanted = filterNode.isWantedAction({
      trace: {
        act: {
          account: 'contract2',
          name: 'action2'
        }
      }
    });
    expect(isWanted).toBeFalsy()

  });
});


describe('Tables filter', () => {
  test('TablePrimaryKeys', async () => {
    let isWanted = filterNode.isWantedTable({
      kvo: {
        code: 'contract1',
        table: 'table1',
      }
    });
    expect(isWanted).toBeFalsy();

    isWanted = filterNode.isWantedTable({
      kvo: {
        code: 'contract2',
        table: 'table1',
      }
    });
    expect(isWanted).toBeTruthy();

    isWanted = filterNode.isWantedTable({
      kvo: {
        code: 'contract2',
        table: 'table2'
      }
    });
    expect(isWanted).toBeTruthy();

    isWanted = filterNode.isWantedTable({
      kvo: {
        code: 'contract3',
        table: 'table3'
      }
    });
    expect(isWanted).toBeFalsy();

  });

});
