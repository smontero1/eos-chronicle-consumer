const straw = require('@smontero/straw');
const { MsgType } = require('../const');
const { DbUpdater } = require('../domain');
const { DbClient, Redis } = require('../service');

module.exports = straw.node({

  async initialize(opts, done) {
    this.opts = opts;
    const {
      redis,
      config: {
        db,
      },
    } = opts;
    this.redis = new Redis(redis);
    this.dbClient = new DbClient(db);
    await this.dbClient.connect();
    this.dbUpdater = new DbUpdater(this.dbClient);
    done();
  },

  async stop(done) {
    console.log("Stopping UpdateTables...");
    await this.dbClient.close();
    done(false);
  },

  async process(data, done) {
    const {
      blockEvent: {
        msgType,
        payload: {
          block_num,
          last_irreversible
        }
      },
      msgs: deltas
    } = data;
    console.log('Message recieved:', JSON.stringify(data, null, 4));
    switch (msgType) {
      case MsgType.BLOCK_COMPLETED:
        await this.dbUpdater.processDeltas(block_num, last_irreversible, deltas);
        await this.dbUpdater.deleteIrreversible(last_irreversible);
        break;
      case MsgType.FORK:
        await this.dbUpdater.rollback(block_num);
        break;
    }
    done(false);
  },
});