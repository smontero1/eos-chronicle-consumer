/* eslint-disable no-undef */
const GroupInBlocks = require('./GroupInBlocks');
const { MsgType } = require('../const');

jest.setTimeout(20000);

let groupInBlocksNode = null;
let outputActionsMock = null;
let outputTableDeltasMock = null;
beforeAll(() => {
  outputActionsMock = jest.fn();
  outputTableDeltasMock = jest.fn();
  groupInBlocksNode = new GroupInBlocks();
  groupInBlocksNode.outputActions = outputActionsMock;
  groupInBlocksNode.outputTableDeltas = outputTableDeltasMock;
});

beforeEach(() => {
  outputActionsMock.mockClear();
  outputTableDeltasMock.mockClear();
});

describe('Test process function', () => {

  test('No actions no deltas', async () => {

    const blockCompletedMsg = {
      msgType: MsgType.BLOCK_COMPLETED,
      payload: {
        block_num: 1,
      }
    };

    groupInBlocksNode.process(
      blockCompletedMsg,
      () => { }
    );

    expect(outputActionsMock.mock.calls.length).toBe(0);
    expect(outputTableDeltasMock.mock.calls.length).toBe(0);

  });

  test('One action message and one delta message', async () => {
    const actionMsg = {
      msgType: MsgType.ACTION,
      payload: {
        block_num: 1,
        trace: {
          account: 'contract1',
          name: 'action1'
        }
      },
    };

    const tableDeltaMsg = {
      msgType: MsgType.TABLE_ROW,
      payload: {
        block_num: 1,
        kvo: {
          code: 'contract1',
          table: 'table1',
          value: 'val1',
        }
      },
    };

    const blockCompletedMsg = {
      msgType: MsgType.BLOCK_COMPLETED,
      payload: {
        block_num: 1,
      }
    };
    groupInBlocksNode.process(
      actionMsg,
      () => { }
    );

    groupInBlocksNode.process(
      tableDeltaMsg,
      () => { }
    );

    groupInBlocksNode.process(
      blockCompletedMsg,
      () => { }
    );

    expect(outputActionsMock.mock.calls.length).toBe(1);
    expect(outputTableDeltasMock.mock.calls.length).toBe(1);
    expect(outputActionsMock.mock.calls[0][0]).toEqual(blockCompletedMsg);
    expect(outputActionsMock.mock.calls[0][1]).toEqual([actionMsg.payload]);
    expect(outputTableDeltasMock.mock.calls[0][0]).toEqual(blockCompletedMsg);
    expect(outputTableDeltasMock.mock.calls[0][1]).toEqual(
      {
        'contract1-table1': [tableDeltaMsg.payload]
      }
    );

  });

  test('Multiple action messages and multiple delta message', async () => {
    const actionMsg1 = {
      msgType: MsgType.ACTION,
      payload: {
        block_num: 1,
        trace: {
          account: 'contract1',
          name: 'action1'
        }
      },
    };

    const actionMsg2 = {
      msgType: MsgType.ACTION,
      payload: {
        block_num: 1,
        trace: {
          account: 'contract1',
          name: 'action2'
        }
      },
    };

    const tableDeltaMsg1 = {
      msgType: MsgType.TABLE_ROW,
      payload: {
        block_num: 1,
        kvo: {
          code: 'contract1',
          table: 'table1',
          value: 'val1',
        }
      },
    };

    const tableDeltaMsg2 = {
      msgType: MsgType.TABLE_ROW,
      payload: {
        block_num: 1,
        kvo: {
          code: 'contract1',
          table: 'table1',
          value: 'val2',
        }
      },
    };

    const tableDeltaMsg3 = {
      msgType: MsgType.TABLE_ROW,
      payload: {
        block_num: 1,
        kvo: {
          code: 'contract1',
          table: 'table2',
          value: 'val1',
        }
      },
    };

    const tableDeltaMsg4 = {
      msgType: MsgType.TABLE_ROW,
      payload: {
        block_num: 1,
        kvo: {
          code: 'contract2',
          table: 'table1',
          value: 'val1',
        }
      },
    };

    const blockCompletedMsg = {
      msgType: MsgType.BLOCK_COMPLETED,
      payload: {
        block_num: 1,
      }
    };
    groupInBlocksNode.process(
      actionMsg1,
      () => { }
    );

    groupInBlocksNode.process(
      tableDeltaMsg1,
      () => { }
    );

    groupInBlocksNode.process(
      actionMsg2,
      () => { }
    );

    groupInBlocksNode.process(
      tableDeltaMsg2,
      () => { }
    );

    groupInBlocksNode.process(
      tableDeltaMsg3,
      () => { }
    );

    groupInBlocksNode.process(
      tableDeltaMsg4,
      () => { }
    );

    groupInBlocksNode.process(
      blockCompletedMsg,
      () => { }
    );

    expect(outputActionsMock.mock.calls.length).toBe(1);
    expect(outputTableDeltasMock.mock.calls.length).toBe(1);
    expect(outputActionsMock.mock.calls[0][0]).toEqual(blockCompletedMsg);
    expect(outputActionsMock.mock.calls[0][1]).toEqual(
      [
        actionMsg1.payload,
        actionMsg2.payload
      ]);
    expect(outputTableDeltasMock.mock.calls[0][0]).toEqual(blockCompletedMsg);
    expect(outputTableDeltasMock.mock.calls[0][1]).toEqual(
      {
        'contract1-table1': [tableDeltaMsg1.payload, tableDeltaMsg2.payload],
        'contract1-table2': [tableDeltaMsg3.payload],
        'contract2-table1': [tableDeltaMsg4.payload],
      }
    );

  });

  test('Fork after One action message and one delta message', async () => {
    const actionMsg = {
      msgType: MsgType.ACTION,
      payload: {
        block_num: 1,
        trace: {
          account: 'contract1',
          name: 'action1'
        }
      },
    };

    const tableDeltaMsg = {
      msgType: MsgType.TABLE_ROW,
      payload: {
        block_num: 1,
        kvo: {
          code: 'contract1',
          table: 'table1',
          value: 'val1',
        }
      },
    };

    const forkMsg = {
      msgType: MsgType.FORK,
      payload: {
        block_num: 1,
      }
    };
    groupInBlocksNode.process(
      actionMsg,
      () => { }
    );

    groupInBlocksNode.process(
      tableDeltaMsg,
      () => { }
    );

    groupInBlocksNode.process(
      forkMsg,
      () => { }
    );

    expect(outputActionsMock.mock.calls.length).toBe(1);
    expect(outputTableDeltasMock.mock.calls.length).toBe(1);
    expect(outputActionsMock.mock.calls[0][0]).toEqual(forkMsg);
    expect(outputActionsMock.mock.calls[0][1]).toBeUndefined();
    expect(outputTableDeltasMock.mock.calls[0][0]).toEqual(forkMsg);
    expect(outputTableDeltasMock.mock.calls[0][1]).toBeUndefined();

    const actionMsg2 = {
      msgType: MsgType.ACTION,
      payload: {
        block_num: 1,
        trace: {
          account: 'contract2',
          name: 'action1'
        }
      },
    };

    const tableDeltaMsg2 = {
      msgType: MsgType.TABLE_ROW,
      payload: {
        block_num: 1,
        kvo: {
          code: 'contract2',
          table: 'table1',
          value: 'val1',
        }
      },
    };

    const blockCompletedMsg = {
      msgType: MsgType.BLOCK_COMPLETED,
      payload: {
        block_num: 1,
      }
    };
    groupInBlocksNode.process(
      actionMsg2,
      () => { }
    );

    groupInBlocksNode.process(
      tableDeltaMsg2,
      () => { }
    );

    groupInBlocksNode.process(
      blockCompletedMsg,
      () => { }
    );

    expect(outputActionsMock.mock.calls.length).toBe(2);
    expect(outputTableDeltasMock.mock.calls.length).toBe(2);
    expect(outputActionsMock.mock.calls[1][0]).toEqual(blockCompletedMsg);
    expect(outputActionsMock.mock.calls[1][1]).toEqual(
      [
        actionMsg2.payload
      ]);
    expect(outputTableDeltasMock.mock.calls[1][0]).toEqual(blockCompletedMsg);
    expect(outputTableDeltasMock.mock.calls[1][1]).toEqual(
      {
        'contract2-table1': [tableDeltaMsg2.payload],
      }
    );

  });

  test('Different block num, before completed msg', async () => {
    const actionMsg = {
      msgType: MsgType.ACTION,
      payload: {
        block_num: 1,
        trace: {
          account: 'contract1',
          name: 'action1'
        }
      },
    };

    const tableDeltaMsg = {
      msgType: MsgType.TABLE_ROW,
      payload: {
        block_num: 1,
        kvo: {
          code: 'contract1',
          table: 'table1',
          value: 'val1',
        }
      },
    };

    const forkMsg = {
      msgType: MsgType.FORK,
      payload: {
        block_num: 1,
      }
    };
    groupInBlocksNode.process(
      actionMsg,
      () => { }
    );

    groupInBlocksNode.process(
      tableDeltaMsg,
      () => { }
    );

    const actionMsg2 = {
      msgType: MsgType.ACTION,
      payload: {
        block_num: 2,
        trace: {
          account: 'contract2',
          name: 'action1'
        }
      },
    };

    const tableDeltaMsg2 = {
      msgType: MsgType.TABLE_ROW,
      payload: {
        block_num: 2,
        kvo: {
          code: 'contract2',
          table: 'table1',
          value: 'val1',
        }
      },
    };

    const blockCompletedMsg = {
      msgType: MsgType.BLOCK_COMPLETED,
      payload: {
        block_num: 2,
      }
    };
    groupInBlocksNode.process(
      actionMsg2,
      () => { }
    );

    groupInBlocksNode.process(
      tableDeltaMsg2,
      () => { }
    );

    groupInBlocksNode.process(
      blockCompletedMsg,
      () => { }
    );

    expect(outputActionsMock.mock.calls.length).toBe(1);
    expect(outputTableDeltasMock.mock.calls.length).toBe(1);
    expect(outputActionsMock.mock.calls[0][0]).toEqual(blockCompletedMsg);
    expect(outputActionsMock.mock.calls[0][1]).toEqual(
      [
        actionMsg2.payload
      ]);
    expect(outputTableDeltasMock.mock.calls[0][0]).toEqual(blockCompletedMsg);
    expect(outputTableDeltasMock.mock.calls[0][1]).toEqual(
      {
        'contract2-table1': [tableDeltaMsg2.payload],
      }
    );

  });
});

