const config = require('config');
const { BaseDao } = require('../dao');
const { TableManager } = require('../domain');
const { DbClient, eosRpc } = require('../service');

class TableInitialStateLoader {

  constructor() {
    const {
      db,
      filters: {
        tables,
      }
    } = config;
    this.dbClient = new DbClient(db);
    this.tablesToLoad = tables;
  }
  async load() {
    try {
      console.log('Strating Table Inital State process...');
      await this.dbClient.connect();
      for (const account in this.tablesToLoad) {
        let tables = null;
        let tableScopeMap = true;
        if (this.tablesToLoad[account] === true) {
          try {
            tables = await eosRpc.getTableNames(account);
          } catch (err) {
            console.log(err)
            console.warn(`${account} not found`);
            continue;
          }
        } else {
          tableScopeMap = this.tablesToLoad[account];
          tables = Object.keys(tableScopeMap);
        }
        for (const table of tables) {
          let scopes = null;
          if (tableScopeMap === true || tableScopeMap[table] === true) {
            scopes = await eosRpc.getTableScopeValues(account, table);
          } else {
            scopes = tableScopeMap[table];
          }
          await this._loadTable({
            account,
            table,
            primaryKey: TableManager.getTablePrimaryKey(account, table),
            scopes
          });

          // try {
          //   TableManager.getTablePrimaryKey(account, table)
          // } catch (error) {
          //   console.log(error)
          // }
        }
      }

    } catch (error) {
      console.error("Table initial state loading failed: ", error);
      throw error;
    } finally {
      await this.dbClient.close();
    }
  }

  async _loadTable({
    account,
    table,
    primaryKey,
    scopes
  }) {

    const baseDao = new BaseDao(TableManager.getTableName(account, table), this.dbClient);
    await baseDao.recreateCollection();
    console.log(`Loading account: ${account} table: ${table}, primaryKey: ${primaryKey}`);
    for (const scope of scopes) {
      console.log(`Loading account: ${account} table: ${table} scope: ${scope}...`);
      let rows = null;
      let more = null;
      let lowerBound = null;
      do {
        ({ rows, more } = await eosRpc.getTableRows(
          {
            code: account,
            table: table,
            scope,
            lowerBound,
            limit: 1000,
          }
        ));
        if (lowerBound) {
          rows.shift();
        }
        if (more) {
          lowerBound = rows[rows.length - 1][primaryKey];
        }
        for (const row of rows) {
          baseDao.insertBatch(TableManager.restructureItem(scope, primaryKey, row));
        }
      } while (more);
    }
    await baseDao.flushInsertBatch();
    console.log(`Loaded account: ${account} table: ${table}.`);
  }
}

new TableInitialStateLoader().load();