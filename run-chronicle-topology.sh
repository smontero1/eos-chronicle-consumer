#!/usr/bin/env bash

export NODE_ENV=$1
echo $NODE_ENV
cd "${BASH_SOURCE%/*}" || exit
nohup node src/topology/ChronicleTopology.js -p >> ./logs/consumer-stdout.log 2>> ./logs/consumer-stderr.log &